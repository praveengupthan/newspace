export interface SpaceItemsModel {
    id: number;
    imgName: any;
    itemTitle: string;
    missionId: number;
    launchYear: number;
    successfulLaunch: string;
    successfulLanding: string;
}
