import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { SpaceItemsModel } from './space.modal';


@Injectable({
  providedIn: 'root'
})
export class SpaceServiceService {

  constructor() { }

  spaceList: SpaceItemsModel[] = [
    {
      id: 1,
      imgName: 'spacex01',
      itemTitle: 'Falcon 9 ABS',
      missionId: 25645,
      launchYear: 2006,
      successfulLaunch: 'Yes',
      successfulLanding: 'No'
    },
    {
      id: 2,
      imgName: 'spacex02',
      itemTitle: 'Descover Falcon 9',
      missionId: 25646,
      launchYear: 2006,
      successfulLaunch: 'Yes',
      successfulLanding: 'No'
    },
    {
      id: 3,
      imgName: 'spacex03',
      itemTitle: 'Falcon 2018 Heavy LC',
      missionId: 25647,
      launchYear: 2007,
      successfulLaunch: 'Yes',
      successfulLanding: 'No'
    },
    {
      id: 4,
      imgName: 'spacex04',
      itemTitle: 'OTV 5F9',
      missionId: 25648,
      launchYear: 2008,
      successfulLaunch: 'Yes',
      successfulLanding: 'No'
    },
    {
      id: 5,
      imgName: 'spacex05',
      itemTitle: 'Hurley Behnken DM2',
      missionId: 25649,
      launchYear: 2009,
      successfulLaunch: 'Yes',
      successfulLanding: 'No'
    },
    {
      id: 6,
      imgName: 'spacex06',
      itemTitle: 'Falcon Heavy ARABSAT - 6A',
      missionId: 25650,
      launchYear: 2010,
      successfulLaunch: 'Yes',
      successfulLanding: 'No'
    },
    {
      id: 7,
      imgName: 'spacex07',
      itemTitle: 'The Falcon HAS Landed',
      missionId: 25651,
      launchYear: 2011,
      successfulLaunch: 'Yes',
      successfulLanding: 'No'
    },
    {
      id: 8,
      imgName: 'spacex08',
      itemTitle: 'SES -9 Spacex',
      missionId: 25652,
      launchYear: 2012,
      successfulLaunch: 'Yes',
      successfulLanding: 'No'
    }
  ];

  getSpaceList(): Observable<SpaceItemsModel[]> {
    return of([...this.spaceList]);
  }
}
