import { Component, OnInit } from '@angular/core';
import { SpaceItemsModel } from './space.modal';
import { SpaceServiceService } from './space-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private spacevar: SpaceServiceService, private route: ActivatedRoute) {

  }
  titleMain = 'SpaceX Launch Program';

  spaceItems: SpaceItemsModel[] = [];
  filterSpaceItems: SpaceItemsModel[];


  filterYears = [
    2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019
  ];




  ngOnInit(): void {
    this.spacevar.getSpaceList().subscribe(data => {
      this.spaceItems = data;
      this.filterSpaceItems = data;
    });
  }

  filterYear(year: number): SpaceItemsModel[] {

    this.spaceItems =  [...this.filterSpaceItems.filter(items => items.launchYear === year)];

    console.log(this.spaceItems);

    return this.spaceItems;
  }




}
